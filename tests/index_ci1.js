/// index.js ///
var exec = require('child_process').exec;
const express = require('express')
const bodyParser = require('body-parser')
var googleTTS = require('google-tts-api');
var SerialPort = require('serialport')

var prevSession = -1
var newSession = -1
var interaction_counter = 0
var start = -1

var port = new SerialPort('/dev/ttyUSB0', {
  baudRate: 9600
});


////////////////////////

function onexit(err, stdout, stderr) {
  if (err) {
    console.log("err");
  }
  console.log(stdout);
}

////////////////////////

const app = express()
app.use(bodyParser.json())
app.set('port', (process.env.PORT || 5000))


////////////////////////

app.post('/webhook', function (req, res) {
  // we expect to receive JSON data from api.ai here.serialport
  // the payload is stored on req.body
console.log("<BODY>")
console.log(req.body)
console.log("</BODY>")

  var webhookReply = req.body.result.fulfillment['speech']

  var uint8 = new Uint8Array(1);
  uint8[0] = 100;
  port.write(uint8)

  // the most basic response
  res.status(200).json({
    source: 'webhook',
    speech: webhookReply,
    displayText: webhookReply
  })


  //var texto = 'hola me llamo andreu y soy un calamar que se cuece a fuego lento en una olla de inducción cableada al nivel de salinidad'

  var request = req.body.result.resolvedQuery

  googleTTS(request, 'es', 1)   // speed normal = 1 (default), slow = 0.24
  .then(function (url) {
    console.log(url); // https://translate.google.com/translate_tts?...
    var call = "cvlc --play-and-exit '" + url + "'"

    const e =  exec(call, onexit);
    e.on('exit', onexit);
  })
  .catch(function (err) {
    console.error(err.stack);
  });

})

app.listen(app.get('port'), function () {
  console.log('* Webhook service is listening on port:' + app.get('port'))
})
