/// index.js ///

const converses = require('./converses.json')
var trigger

var iso = require('iso-8859-15')
var rd = require('diacritics').remove

const express = require('express')
const bodyParser = require('body-parser')

const SerialPort = require('serialport')

const sayp = require("say-promise");
var voiceName

var prevSession = -1
var newSession = -1
var interaction_counter = 0
var start = -1

var port = new SerialPort('/dev/ttyUSB0', {
  baudRate: 9600
});


////////////////////////

function onexit(err, stdout, stderr) {
  if (err) {
    console.log("err");
  }
  console.log(stdout);
}

////////////////////////

const app = express()
app.use(bodyParser.json())
app.set('port', (process.env.PORT || 5000))


////////////////////////

app.post('/webhook', function (req, res) {
  
  // the payload from dialogflow is stored on req.body
  console.log("<BODY>")
  console.log(req.body)
  console.log("</BODY>")

  // parse incomming json
  var qR = req.body.queryResult
  var query  = qR.queryText
  var response = qR.fulfillmentText
  var contexts = qR.outputContexts

  // send data to arduino over serial
  var uint8 = new Uint8Array(1)
  uint8[0] = 100;
  port.write(uint8)

  // prepare webhook response
  res.status(200).json({
    fullfillmentText: response,
    source: 'webhook',
    outputContexts: contexts,
  })


  // encode texts
  var q = rd(query)			//iso.encode(rd(query)) 		//iso.encode(query)
  var r = rd(response)			//iso.encode(rd(response))      	//iso.encode(response)


  sonifyConv([q,r], qR.languageCode)

})

function triggerConversation(){
  var t = 30000 + Math.floor(Math.random()*10000)
  console.log("next trigger in... "+ t/1000 + " seconds");

  var i = Math.floor(Math.random()* converses.length)
  
  trigger = setTimeout(()=>sonifyConv(converses[i].c, converses[i].l) , t) // random betwwen 30 and 40 seconds
}

function light(){
  var uint8 = new Uint8Array(1)
  uint8[0] = 100;
  port.write(uint8)
}


function sonifyConv(c, l){
  console.log("sonifying conversation")
  console.log(l + ">" + c)

  var voiceName1
  var voiceName2
  if (l == "es"){
	voiceName1 = "voice_el_diphone"
	voiceName2 = "voice_JuntaDeAndalucia_es_sf_diphone"
  }
  else if (l == "fr"){
	voiceName1 = "voice_upc_ca_ona_hts"
	voiceName2 = "voice_upc_ca_teo_hts"
  }
  else{
	voiceName1 = "voice_kal_diphone"
	voiceName2 = "voice_rab_diphone"
  }

  sayp.speak(c[0], voiceName1, 1).then(
	()=>{light();sayp.speak(c[1], voiceName2, 1)}).then(
	()=>{if(c[2]){light();sayp.speak(c[2], voiceName1, 1)}}).then(
	()=>{if(c[3]){light();sayp.speak(c[3], voiceName2, 1)}}).then(
	()=>{clearTimeout(trigger); triggerConversation()}).catch(
	(err)=>console.log(err))
}


app.listen(app.get('port'), function () {
  console.log('* Webhook service is listening on port:' + app.get('port'))

  triggerConversation();
})
